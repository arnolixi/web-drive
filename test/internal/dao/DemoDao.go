package dao

import arc "gitee.com/arnolixi/web-drive"

type DemoDao struct {
	DB *arc.GormAdapter
}

func (D *DemoDao) Name() string {
	return "DemoDao"
}