package services

import "gitee.com/arnolixi/web-drive/test/internal/dao"

type DemoService struct {
	DAO *dao.DemoDao
}

func (S *DemoService) Name() string {
	return "DemoService"
}