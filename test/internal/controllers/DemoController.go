package controllers

import (
	arc "gitee.com/arnolixi/web-drive"
	"gitee.com/arnolixi/web-drive/test/internal/services"
	"github.com/gin-gonic/gin"
)

type DemoController struct {
	
	Srv *services.DemoService
}

func NewDemoController() *DemoController {
	return &DemoController{}
}

func (C *DemoController) Name() string {
	return "DemoController"
}

func (C *DemoController) get(ctx *gin.Context) arc.Json {
	
	return gin.H{}
}

func (C *DemoController) Build(app *arc.Reactor) {
	app.Handle("GET", "demo", C.get)

}
