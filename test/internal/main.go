package main

import (
	arc "gitee.com/arnolixi/web-drive"
	"gitee.com/arnolixi/web-drive/test/internal/controllers"
)

func main()  {

	app:=arc.Start()
	app.Mount("/api/v1",controllers.NewDemoController())
	app.GoRun()
}


