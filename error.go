package arc

type Error interface {
	// 设置状态码
	SetStatus(int) Error
	// 获取状态码
	GetStatus() int
	// 设置返回结果
	SetData(interface{}) Error
	// 获取返回结果
	GetData() interface{}
	// 设置error
	SetError(err error) Error
	// 直接使用string 设置error
	SetErrWithStr(err string) Error
	// 继承error接口中的Error
	Error() string
	// 获取error
	GetError() error
	// 获取Error Json处理对象
	JSON() interface{}
	MarshalJSON() ([]byte, error)
	Unwrap() interface{}
	UnwrapOr(v interface{}) interface{}
	UnwrapOrElse(f func() interface{}) interface{}
}
